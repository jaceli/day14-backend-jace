package com.todo.todolist.exceptions;

public class TodoNotFoundException extends RuntimeException {
    public TodoNotFoundException() {
        super("todo is not found");
    }
}
