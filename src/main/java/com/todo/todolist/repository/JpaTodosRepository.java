package com.todo.todolist.repository;

import com.todo.todolist.Entity.Todo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JpaTodosRepository extends JpaRepository<Todo, Long> {
}
