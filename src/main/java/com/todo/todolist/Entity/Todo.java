package com.todo.todolist.Entity;

import lombok.Builder;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "todos")
public class Todo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    String text;

    boolean done;

    public Todo() {
    }

    public Todo(Long id, String text, boolean done) {
        this.id = id;
        this.text = text;
        this.done = false;
    }
}
