package com.todo.todolist.service.mapper;

import com.todo.todolist.Entity.Todo;
import com.todo.todolist.service.dto.TodoResponse;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class TodoMapper {

    public static TodoResponse toResponse(Todo todo) {
        TodoResponse todoResponse = new TodoResponse();
        BeanUtils.copyProperties(todo, todoResponse);
        return todoResponse;
    }

    public static List<TodoResponse> toResponseList(List<Todo> todos) {
        return todos.stream().map(todo -> toResponse(todo)).collect(Collectors.toList());
    }
}
