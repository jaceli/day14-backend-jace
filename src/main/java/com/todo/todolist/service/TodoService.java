package com.todo.todolist.service;

import com.todo.todolist.Entity.Todo;
import com.todo.todolist.repository.JpaTodosRepository;
import com.todo.todolist.service.mapper.TodoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import com.todo.todolist.exceptions.TodoNotFoundException;
@Service
public class TodoService {

    @Autowired
    private JpaTodosRepository jpaTodosRepository;
    public List<Todo> getTodos() {
        return jpaTodosRepository.findAll();
    }

    public Todo createTodo(Todo todo) {
        Todo newTodo = new Todo();
        newTodo.setText(todo.getText());
        jpaTodosRepository.save(newTodo);
        return newTodo;
    }

    public void deleteTodo(Long id) {
        jpaTodosRepository.deleteById(id);
    }

    public Todo updateTodo(Long id, Todo todo) {
        Todo previousTodo = jpaTodosRepository.findById(id).orElseThrow(TodoNotFoundException::new);
        if(Objects.nonNull(todo.getText())) {
            previousTodo.setText(todo.getText());
        }
        if(Objects.nonNull(todo.isDone())) {
            previousTodo.setDone(todo.isDone());
        }
        jpaTodosRepository.save(previousTodo);
        return previousTodo;
    }

    public Todo getTodoById(Long id) {
        return jpaTodosRepository.findById(id).orElseThrow(TodoNotFoundException::new);
    }
}
