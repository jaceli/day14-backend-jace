package com.todo.todolist.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.todo.todolist.Entity.Todo;
import com.todo.todolist.repository.JpaTodosRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.Random.class)
public class TodoControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private JpaTodosRepository jpaTodosRepository;

    @BeforeEach
    public void setUp() {
        jpaTodosRepository.deleteAll();
    }

    @Test
    void should_find_todos() throws Exception {
        Todo todo = new Todo(null, "test hhh", false);
        jpaTodosRepository.save(todo);

        mockMvc.perform(get("/todos"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].text").value(todo.getText()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].done").value(todo.isDone()));
    }

    @Test
    void should_create_todo() throws Exception {
        Todo todo = new Todo(null, "foo", false);

        ObjectMapper objectMapper = new ObjectMapper();
        String todoJSON = objectMapper.writeValueAsString(todo);
        mockMvc.perform(post("/todos")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(todoJSON))
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath(("$.text")).value("foo"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(false));
    }

    @Test
    void should_delete_todo() throws Exception {
        Todo todo = new Todo(null, "test", false);
        jpaTodosRepository.save(todo);

        mockMvc.perform(delete("/todos/{id}", todo.getId()))
                .andExpect(MockMvcResultMatchers.status().is(204));

        assertFalse(jpaTodosRepository.findById(todo.getId()).isPresent());
    }

    @Test
    void should_update_todo() throws Exception {
        Todo todo = new Todo(null, "test", false);
        jpaTodosRepository.save(todo);

        Todo newTodo = new Todo(null, "xiugai", true);
        ObjectMapper objectMapper = new ObjectMapper();
        String todoJSON = objectMapper.writeValueAsString(newTodo);
        mockMvc.perform(put("/todos/{id}", todo.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(todoJSON))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath(("$.text")).value(newTodo.getText()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(newTodo.isDone()));
    }

    @Test
    void should_find_todo_by_id() throws Exception {
        Todo todo = new Todo(null, "test hhh", false);
        jpaTodosRepository.save(todo);

        mockMvc.perform(get("/todos/{id}", todo.getId()))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value(todo.getText()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(todo.isDone()));
    }

    @Test
    void should_throw_exception_find_todo_by_id_when_todo_is_not_found() throws Exception {
        mockMvc.perform(get("/todos/{id}", 999))
                .andExpect(MockMvcResultMatchers.status().is(404))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("todo is not found"));
    }
}
