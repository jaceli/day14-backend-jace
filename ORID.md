# ORID



## Objective

- When I conducted the code review today, I mainly learned how to beautify the user interface when using ant design.
- In addition, today I mainly learned the combination of front and back ends, wrote the interfaces myself, and then called these interfaces in the front end, which enabled me to combine the knowledge points of the front and back ends and consolidated the spring boot I had learned before.
- In the afternoon, we made a summary of React we learned recently by drawing concept maps. At the same time, our various groups also gave presentations on agile development.



## Reflective

- fruitful and challenging


## Interpretative

- Today, I learned the practice of combining the front and back ends, and adjusted the interface written by myself, which gave me a great sense of accomplishment. At the same time, full-stack development has become more challenging for me.

## Decision

- I will continue to study hard in the next course, and I will continue to learn front-end knowledge and finish my homework on time.